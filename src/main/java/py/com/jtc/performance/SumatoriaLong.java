package py.com.jtc.performance;

/**
 * Ejemplo de creacion de objetos innecesarios por Autoboxing
 */
public class SumatoriaLong {
	
	public long sumaIncremental(long dato) {
		Long suma = 0L;
		suma = suma + dato; //dato obligara a crear un objeto nuevo en cada paso
		return suma;
	}

	public static void main(String[] args) throws InterruptedException {
		SumatoriaLong sumador = new SumatoriaLong();
		Long resultado = 0L;
		for (long i=0; i < Long.MAX_VALUE; i++) {
			resultado = resultado + sumador.sumaIncremental(i);
			Thread.sleep(500);
		}
	}

}
